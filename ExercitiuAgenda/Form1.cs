﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Agenda
{
    public partial class FormAgenda : Form
    {

        public List<Person> personList;
        public Person globalPerson;


        public FormAgenda()
        {
            InitializeComponent();
        }

        private void groupBox_persons_Enter(object sender, EventArgs e)
        {

        }

        private void FormAgenda_Load(object sender, EventArgs e)
        {
            personList = new List<Person>();

            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            if (!Directory.Exists(path + "\\EBS Course"))
                Directory.CreateDirectory((path + "\\EBS Course"));
            if (!File.Exists(path + "\\EBS Course\\Agenda.txt"))
                File.Create((path + "\\EBS Course\\Agenda.txt"));

            this.dataGridView_personslist.MultiSelect = false;
            this.dataGridView_personslist.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView_personslist.ClearSelection();
        }

        private void btn_AddPerson_Click(object sender, EventArgs e)
        {
            String email = textBox_email.Text;
            String phone = textBox_telephone.Text;
            String name = textBox_name.Text;

            Person pers = new Person(name, email, phone);

            if (email.Length > 0 && phone.Length > 0 && name.Length > 0)
            {
                personList.Add(pers);
                this.dataGridView_personslist.Rows.Add(name, email, phone);
            }

            resetFields();
        }

        private void resetFields()
        {
            textBox_name.Text = "";
            textBox_telephone.Text = "";
            textBox_email.Text = "";
        }



        private int findPerson(Person person)
        {
            int index = 0;
            foreach (Person p in personList)
            {
                if (person.name == p.name && person.phone == p.phone && person.email == p.email)
                {
                    return index;
                }

                index++;
            }

            return -1;
        }

        private void btn_saveChanges_Click(object sender, EventArgs e)
        {
            String email = textBox_email.Text;
            String phone = textBox_telephone.Text;
            String name = textBox_name.Text;

            Person newPerson = new Person(name, email, phone);

            if (email.Length > 0 && phone.Length > 0 && name.Length > 0)
            {
                int index = findPerson(globalPerson);

                if (index != -1)
                {
                    personList[index] = newPerson;
                    this.dataGridView_personslist.Rows[index].SetValues(name, email, phone);
                }
            }
        }

        private void dataGridView_personslist_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btn_RemovePersons_Click(object sender, EventArgs e)
        {
            int index = findPerson(globalPerson);

            if (index != -1)
            {
                personList.RemoveAt(index);

                this.dataGridView_personslist.Rows[index].Visible = false;
            }
        }

        private void dataGridView_personslist_SelectionChanged(object sender, EventArgs e)
        {
            if (this.dataGridView_personslist.SelectedRows.Count > 0)
            {

                DataGridViewRow row = this.dataGridView_personslist.SelectedRows[0];
                String name = row.Cells[0].Value.ToString();
                String email = row.Cells[1].Value.ToString();
                String phone = row.Cells[2].Value.ToString();

                globalPerson = new Person(name, email, phone);

                this.textBox_telephone.Text = phone;
                this.textBox_email.Text = email;
                this.textBox_name.Text = name;
            }
        }
    }
}
