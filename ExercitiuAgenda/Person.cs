﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agenda
{
     public class Person
    {
        public String name;
        public String phone;
        public String email;

        public Person(String name, String email, String phone)
        {
            this.phone = phone;
            this.email = email;
            this.name = name;
        }
    }
}
