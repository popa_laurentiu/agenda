﻿namespace Agenda
{
    partial class FormAgenda
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox_persons = new System.Windows.Forms.GroupBox();
            this.dataGridView_personslist = new System.Windows.Forms.DataGridView();
            this.groupBox_informations = new System.Windows.Forms.GroupBox();
            this.btn_saveChanges = new System.Windows.Forms.Button();
            this.btn_RemovePersons = new System.Windows.Forms.Button();
            this.btn_AddPerson = new System.Windows.Forms.Button();
            this.textBox_email = new System.Windows.Forms.TextBox();
            this.textBox_telephone = new System.Windows.Forms.TextBox();
            this.textBox_name = new System.Windows.Forms.TextBox();
            this.label_email = new System.Windows.Forms.Label();
            this.label_telephone = new System.Windows.Forms.Label();
            this.label_name = new System.Windows.Forms.Label();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox_persons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_personslist)).BeginInit();
            this.groupBox_informations.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox_persons
            // 
            this.groupBox_persons.Controls.Add(this.dataGridView_personslist);
            this.groupBox_persons.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox_persons.Location = new System.Drawing.Point(0, 0);
            this.groupBox_persons.Name = "groupBox_persons";
            this.groupBox_persons.Size = new System.Drawing.Size(435, 421);
            this.groupBox_persons.TabIndex = 0;
            this.groupBox_persons.TabStop = false;
            this.groupBox_persons.Text = "Persons";
            this.groupBox_persons.Enter += new System.EventHandler(this.groupBox_persons_Enter);
            // 
            // dataGridView_personslist
            // 
            this.dataGridView_personslist.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_personslist.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
            this.dataGridView_personslist.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_personslist.Location = new System.Drawing.Point(3, 16);
            this.dataGridView_personslist.Name = "dataGridView_personslist";
            this.dataGridView_personslist.Size = new System.Drawing.Size(429, 402);
            this.dataGridView_personslist.TabIndex = 0;
            this.dataGridView_personslist.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_personslist_CellContentClick);
            this.dataGridView_personslist.SelectionChanged += new System.EventHandler(this.dataGridView_personslist_SelectionChanged);
            // 
            // groupBox_informations
            // 
            this.groupBox_informations.AutoSize = true;
            this.groupBox_informations.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox_informations.Controls.Add(this.btn_saveChanges);
            this.groupBox_informations.Controls.Add(this.btn_RemovePersons);
            this.groupBox_informations.Controls.Add(this.btn_AddPerson);
            this.groupBox_informations.Controls.Add(this.textBox_email);
            this.groupBox_informations.Controls.Add(this.textBox_telephone);
            this.groupBox_informations.Controls.Add(this.textBox_name);
            this.groupBox_informations.Controls.Add(this.label_email);
            this.groupBox_informations.Controls.Add(this.label_telephone);
            this.groupBox_informations.Controls.Add(this.label_name);
            this.groupBox_informations.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.groupBox_informations.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBox_informations.Location = new System.Drawing.Point(459, 0);
            this.groupBox_informations.Name = "groupBox_informations";
            this.groupBox_informations.Size = new System.Drawing.Size(247, 421);
            this.groupBox_informations.TabIndex = 1;
            this.groupBox_informations.TabStop = false;
            this.groupBox_informations.Text = "Informations";
            // 
            // btn_saveChanges
            // 
            this.btn_saveChanges.Location = new System.Drawing.Point(166, 231);
            this.btn_saveChanges.Name = "btn_saveChanges";
            this.btn_saveChanges.Size = new System.Drawing.Size(75, 23);
            this.btn_saveChanges.TabIndex = 7;
            this.btn_saveChanges.Text = "Save";
            this.btn_saveChanges.UseVisualStyleBackColor = true;
            this.btn_saveChanges.Click += new System.EventHandler(this.btn_saveChanges_Click);
            // 
            // btn_RemovePersons
            // 
            this.btn_RemovePersons.Location = new System.Drawing.Point(85, 231);
            this.btn_RemovePersons.Name = "btn_RemovePersons";
            this.btn_RemovePersons.Size = new System.Drawing.Size(75, 23);
            this.btn_RemovePersons.TabIndex = 6;
            this.btn_RemovePersons.Text = "Remove";
            this.btn_RemovePersons.UseVisualStyleBackColor = true;
            this.btn_RemovePersons.Click += new System.EventHandler(this.btn_RemovePersons_Click);
            // 
            // btn_AddPerson
            // 
            this.btn_AddPerson.Location = new System.Drawing.Point(6, 231);
            this.btn_AddPerson.Name = "btn_AddPerson";
            this.btn_AddPerson.Size = new System.Drawing.Size(75, 23);
            this.btn_AddPerson.TabIndex = 1;
            this.btn_AddPerson.Text = "Add Person";
            this.btn_AddPerson.UseVisualStyleBackColor = true;
            this.btn_AddPerson.Click += new System.EventHandler(this.btn_AddPerson_Click);
            // 
            // textBox_email
            // 
            this.textBox_email.Location = new System.Drawing.Point(99, 126);
            this.textBox_email.Name = "textBox_email";
            this.textBox_email.Size = new System.Drawing.Size(136, 20);
            this.textBox_email.TabIndex = 5;
            // 
            // textBox_telephone
            // 
            this.textBox_telephone.Location = new System.Drawing.Point(93, 74);
            this.textBox_telephone.Name = "textBox_telephone";
            this.textBox_telephone.Size = new System.Drawing.Size(136, 20);
            this.textBox_telephone.TabIndex = 4;
            // 
            // textBox_name
            // 
            this.textBox_name.Location = new System.Drawing.Point(93, 42);
            this.textBox_name.Name = "textBox_name";
            this.textBox_name.Size = new System.Drawing.Size(136, 20);
            this.textBox_name.TabIndex = 3;
            // 
            // label_email
            // 
            this.label_email.AutoSize = true;
            this.label_email.Location = new System.Drawing.Point(16, 126);
            this.label_email.Name = "label_email";
            this.label_email.Size = new System.Drawing.Size(32, 13);
            this.label_email.TabIndex = 2;
            this.label_email.Text = "Email";
            // 
            // label_telephone
            // 
            this.label_telephone.AutoSize = true;
            this.label_telephone.Location = new System.Drawing.Point(16, 77);
            this.label_telephone.Name = "label_telephone";
            this.label_telephone.Size = new System.Drawing.Size(58, 13);
            this.label_telephone.TabIndex = 1;
            this.label_telephone.Text = "Telephone";
            // 
            // label_name
            // 
            this.label_name.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_name.AutoSize = true;
            this.label_name.Location = new System.Drawing.Point(16, 42);
            this.label_name.Name = "label_name";
            this.label_name.Size = new System.Drawing.Size(35, 13);
            this.label_name.TabIndex = 0;
            this.label_name.Text = "Name";
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Nume";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Email";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Telephone";
            this.Column3.Name = "Column3";
            // 
            // FormAgenda
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(706, 421);
            this.Controls.Add(this.groupBox_informations);
            this.Controls.Add(this.groupBox_persons);
            this.Name = "FormAgenda";
            this.Text = "Agenda";
            this.Load += new System.EventHandler(this.FormAgenda_Load);
            this.groupBox_persons.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_personslist)).EndInit();
            this.groupBox_informations.ResumeLayout(false);
            this.groupBox_informations.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox_persons;
        private System.Windows.Forms.DataGridView dataGridView_personslist;
        private System.Windows.Forms.GroupBox groupBox_informations;
        private System.Windows.Forms.TextBox textBox_email;
        private System.Windows.Forms.TextBox textBox_telephone;
        private System.Windows.Forms.TextBox textBox_name;
        private System.Windows.Forms.Label label_email;
        private System.Windows.Forms.Label label_telephone;
        private System.Windows.Forms.Label label_name;
        private System.Windows.Forms.Button btn_AddPerson;
        private System.Windows.Forms.Button btn_saveChanges;
        private System.Windows.Forms.Button btn_RemovePersons;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
    }
}

